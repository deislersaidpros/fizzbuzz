/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fizzbuzz;
import java.util.Scanner;
/**
 *
 * @author Said Prado Ospina
 */
public class Fizzbuzz_1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("UFPS-Alternate Solution");
        Scanner in = new Scanner(System.in);
        int num = in.nextInt();
        getFizzbuzz(num);
    }
    public static void getFizzbuzz(int num )
	{
            if(num<=0) throw new RuntimeException("Invalid Data");
            int i=0;
            String salida="";
            while(i<num){
                i++;
                if(i%15==0) salida+=i+" FizzBuzz \n";
                else{
                    if(i%3==0) salida+=i+" Fizz \n";
                    else{
                        if(i%5==0){
                        if(i%3==0) salida+=i+" Buzz \n";
                        }else salida+=i+"\n";
                    }
                }
            }
            System.out.println(salida);
        }
    
}
